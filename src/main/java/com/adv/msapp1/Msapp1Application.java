package com.adv.msapp1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Msapp1Application {

	public static void main(String[] args) {
		SpringApplication.run(Msapp1Application.class, args);
	}
}
