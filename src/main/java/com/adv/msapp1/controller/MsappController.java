package com.adv.msapp1.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MsappController {

	@GetMapping("/test")
	public String testApi() {
		return "Test API - Working fine";
	}
}
